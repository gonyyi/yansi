# YANSI, an ANSI color library for Golang

YANSI is an ANSI library that can control cursor location, clear the screen, and change 
the color of the text in terminal that supports VT100. Currently, it has been tested for 
DOS terminal in Windows 7, and Linux (REHL).

## Usage

### Installation
    go get https://github.com/gonyi/yansi

### Importing the library
    import "https://github.com/gonyi/yansi"

### Initializing
    y := yansi.New()
    y.Init()

### Define
    blueText, _ := y.SetFont("BLUE", "BRIGHT") // For setFont, you can use font option below (2 ~ unlimited)
    blueTextWhiteBackGround, _ := y.SetFont("BLUE", "BG_WHITE")
    cursorName, _ := y.SetCursor(10,10)
    clear := y.Clear()

### Usage
    fmt.Print(
        clear() + 
        blueText("this is blue") + 
        blueTextWhiteBackground("this is blue and white") + 
        cursorName() 
        )

### Font Options

* BLACK
* RED
* GREEN
* YELLOW
* BLUE
* MAGENTA
* CYAN
* WHITE
* BG_BLACK
* BG_RED
* BG_GREEN
* BG_YELLOW
* BG_BLUE
* BG_MAGENTA
* BG_CYAN
* BG_WHITE
* RESET
* BRIGHT
* DIM
* UNDERSCORE
* BLINK
* REVERSE
* HIDDEN

### License

Copyright (c) 2015 Gon Yi

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
