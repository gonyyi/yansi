/* --------------------------------------------------------------------------- *
  Title:		yansi 0.1.0, an ANSI color library for Go

  Author:		Gon Yi (gonyispam@gmail.com)
  Date:			July 24, 2015
  Description:	VT100 based; cross platform (tested on REHL and Windows)
* --------------------------------------------------------------------------- */

package yansi

import (
	"errors"
	"strconv"
)

const (
	esc = "\033"
)

type yansi struct {
	font map[string]string
}

func New() yansi {
	return yansi{}
}

func (g *yansi) Init() {
	g.font = map[string]string{}
	g.font["BLACK"] = "30"
	g.font["RED"] = "31"
	g.font["GREEN"] = "32"
	g.font["YELLOW"] = "33"
	g.font["BLUE"] = "34"
	g.font["MAGENTA"] = "35"
	g.font["CYAN"] = "36"
	g.font["WHITE"] = "37"
	g.font["BG_BLACK"] = "40"
	g.font["BG_RED"] = "41"
	g.font["BG_GREEN"] = "42"
	g.font["BG_YELLOW"] = "43"
	g.font["BG_BLUE"] = "44"
	g.font["BG_MAGENTA"] = "45"
	g.font["BG_CYAN"] = "46"
	g.font["BG_WHITE"] = "47"
	g.font["RESET"] = "0"
	g.font["BRIGHT"] = "1"
	g.font["DIM"] = "2"
	g.font["UNDERSCORE"] = "3"
	g.font["BLINK"] = "4"
	g.font["REVERSE"] = "7"
	g.font["HIDDEN"] = "8"
}

func (g *yansi) SetFont(option ...string) (func(string) string, error) {
	// if fore
	var out string
	for _, val := range option {
		if _, ok := g.font[val]; ok {
			out += g.font[val] + ";"
		} else {
			return nil, errors.New("Invalid color/font code")
		}
	}
	out = out[:len(out)-1]
	fOut := func(a string) string {
		return esc + "[" + out + "m" +
			a + esc + "[" + g.font["RESET"] + "m"
	}
	return fOut, nil
}

func (g *yansi) SetCursor(row, col int) (func() string, error) {
	if row > 0 && col > 0 {
		return func() string {
			return esc + "[" + strconv.Itoa(row) + ";" + strconv.Itoa(col) + "f"
		}, nil
	} else {
		return func() string { return "" },
			errors.New("Outside the cursor location range")
	}
}

func (g *yansi) Clear() func() string {
	return func() string {
		return esc + "[2J" + esc + "[1;1f"
	}
}
